const fizzbuzz = require('./fizzbuzz');

test('fizzbuzz function exists', () =>{
    expect(fizzbuzz).toBeDefined();
});

test('returns 1 for the number 1', () => {
  expect(fizzbuzz(1)).toEqual(1);
});

test('returns 2 for the number 2', () => {
  expect(fizzbuzz(2)).toEqual(2);
});

test('returns "Fizz" for the number 3', () => {
  expect(fizzbuzz(3)).toEqual('Fizz');
});

test('returns "Fizz" for the number 6', () => {
  expect(fizzbuzz(6)).toEqual('Fizz');
});

test('returns "Buzz" for the number 5', () => {
  expect(fizzbuzz(5)).toEqual('Buzz');
});

test('returns "Buzz" for the number 10', () => {
  expect(fizzbuzz(10)).toEqual('Buzz');
});

test('returns "FizzBuzz" for the number 15', () => {
  expect(fizzbuzz(15)).toEqual('FizzBuzz');
});

test('returns "FizzBuzz" for the number 45', () => {
  expect(fizzbuzz(45)).toEqual('FizzBuzz');
});